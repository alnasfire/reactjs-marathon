import React from 'react';
import ReactDom from 'react-dom';
import './index.css'
import HeaderBlock from './components/HeaderBlock';

const AppList = () => {
  return (
    <ul>
      <li>my first list</li>
      <li>my second list</li>
    </ul>
  );
}

const AppHeader = () => {
  const margin = 40;
  const headerStyle = {
    color: 'red',
    marginLeft: `${margin}px`
  }

  return (
    <h1 className="header">My Header</h1>
  );
}

const App = () => {
  return (
    <>
      <HeaderBlock />
      <AppHeader />
      <AppList />
    </>
  );
}

ReactDom.render(<App />, document.getElementById('root'));
